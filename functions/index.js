const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const Twilio = require('twilio');
const sid = functions.config().twilio.sid;
const token = functions.config().twilio.token;

const client = new Twilio(sid, token);

const twilioNumber = '+918286547834';

exports.textStatus = functions.database
    .ref(`/customer/{customerKey}/phone`)
    .onCreate((event) => {
      const customerKey = event.params.customerKey;
      return admin.database()
          .ref(`/customer/${customerKey}`)
          .once('value')
          .then((snapshot) => snapshot.val())
          .then((customer) => {
            const customerName = customer.name;
            const customerPhone = customer.phone;
            const message = {
              body: `Hello ${customerName}`,
              to: customerPhone,
              from: twilioNumber,
            };

            return client.messages.create(message)
                .then((res) => console.log(res))
                .catch((err) => console.log(err));
          });
    });
