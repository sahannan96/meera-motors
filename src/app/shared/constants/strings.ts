export const CUSTOMER = 'customer';
export const DARK_MODE = 'dark_mode';
export const DARK_MODE_CLASS_NAME = 'darkMode';
export const ON = 'on';
export const OFF = 'off';
