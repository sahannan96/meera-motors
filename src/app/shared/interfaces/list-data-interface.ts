export interface Customer {
  uuid?: string;
  name?: string;
  phone?: number;
  bike?: string;
  coins?: number;
  logs?: Logs[];
}

export interface Logs {
  prev_balance?: number;
  amount?: number;
  date?: any;
  new_balance?: number;
  type?: boolean;
}
