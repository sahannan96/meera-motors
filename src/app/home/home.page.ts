import { Component } from '@angular/core';
import {
  collection,
  getFirestore,
  onSnapshot,
  query,
} from 'firebase/firestore';
import { CUSTOMER } from '../shared/constants/strings';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  db = getFirestore();
  totalCustomers: number;
  constructor() {
    this.getCutomersCount();
  }

  getCutomersCount() {
    const docRef = collection(this.db, CUSTOMER);
    const order = query(docRef);
    onSnapshot(order, (snapshot) => {
      this.totalCustomers = snapshot.docs.length;
    });
  }
}
