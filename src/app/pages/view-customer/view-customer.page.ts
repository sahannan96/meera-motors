import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import {
  arrayUnion,
  doc,
  getFirestore,
  onSnapshot,
  updateDoc,
} from 'firebase/firestore';
import { CUSTOMER } from '../../shared/constants/strings';
import { Customer, Logs } from '../../shared/interfaces/list-data-interface';

@Component({
  selector: 'app-view-customer',
  templateUrl: './view-customer.page.html',
  styleUrls: ['./view-customer.page.scss'],
})
export class ViewCustomerPage implements OnInit {
  db = getFirestore();
  customer: Customer = {};
  spinner: boolean = true;
  add: boolean = false;
  remove: boolean = false;

  purchaseCoins = new FormControl();
  redeemCoins = new FormControl();

  constructor(
    private readonly route: ActivatedRoute,
    private readonly alertController: AlertController,
    private readonly toastController: ToastController
  ) {}

  ngOnInit() {
    this.customer.uuid = this.route.snapshot.params.uuid;
    this.loadCustomer();
  }

  loadCustomer() {
    const docRef = doc(this.db, CUSTOMER, this.customer.uuid);
    onSnapshot(docRef, (doc) => {
      this.customer = {};
      this.customer = doc.data();
      this.customer.uuid = doc.id;
      this.spinner = false;
    });
  }

  addCoins() {
    this.presentAlert(
      `Are you sure you want to Purchase ${this.purchaseCoins.value} coins?`
    );
  }

  removeCoins() {
    this.presentAlert(
      `Are you sure you want to Redeem ${this.redeemCoins.value} coins?`
    );
  }

  async presentAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: msg,
      cssClass: 'custom-alert',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'alert-button-cancel',
        },
        {
          text: 'Confirm',
          role: 'confirm',
          cssClass: 'alert-button-confirm',
          handler: () => {
            this.spinner = true;
            let coins = this.customer.coins;
            if (this.add) {
              coins = coins + this.purchaseCoins.value;
            } else {
              coins = coins - this.redeemCoins.value;
            }
            this.updateLogs(coins);
          },
        },
      ],
    });
    await alert.present();
  }

  updateLogs(coins: number) {
    const logs = {} as Logs;
    logs.amount = this.add ? this.purchaseCoins.value : this.redeemCoins.value;
    logs.date = new Date();
    logs.prev_balance = this.customer.coins;
    logs.new_balance = coins;
    logs.type = this.add ? true : false;

    const docRef = doc(this.db, CUSTOMER, this.customer.uuid);
    updateDoc(docRef, { logs: arrayUnion(logs) })
      .then(() => {
        this.updateCoins(coins);
        this.presentToast('Logs Updated');
      })
      .catch((error) => {
        this.presentToast(error);
      });
  }

  updateCoins(coins: number) {
    const docRef = doc(this.db, CUSTOMER, this.customer?.uuid);
    updateDoc(docRef, { coins })
      .then(() => {
        this.purchaseCoins.reset();
        this.redeemCoins.reset();
        this.spinner = false;
        this.presentToast('Coins Updated');
      })
      .catch((error) => {
        this.spinner = false;
        this.presentToast(error);
      });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
