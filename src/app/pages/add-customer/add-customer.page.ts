import { Component, OnInit } from '@angular/core';
import { addDoc, collection, getFirestore } from 'firebase/firestore';
import { FormControl, FormGroup } from '@angular/forms';
import { CUSTOMER } from '../../shared/constants/strings';
import { Customer, Logs } from '../../shared/interfaces/list-data-interface';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.page.html',
  styleUrls: ['./add-customer.page.scss'],
})
export class AddCustomerPage implements OnInit {
  db = getFirestore();
  spinner: boolean = false;
  customerForm = new FormGroup({
    name: new FormControl(),
    phone: new FormControl(),
    bike: new FormControl(),
    coins: new FormControl(),
  });

  get f() {
    return this.customerForm.controls;
  }

  constructor(
    private readonly toastController: ToastController,
    private readonly router: Router
  ) {}

  ngOnInit() {}

  addCustomer() {
    const customer = {} as Customer;
    customer.name = this.f.name.value;
    customer.phone = parseInt(this.f.phone.value);
    customer.bike = this.f.bike.value;
    customer.coins = parseInt(this.f.coins.value);
    customer.logs = [];
    customer.logs.push(this.getLogData());

    const docRef = collection(this.db, CUSTOMER);
    addDoc(docRef, customer)
      .then(() => {
        this.spinner = false;
        this.presentToast('Customer Added');
        this.router.navigate(['/list-customers']);
      })
      .catch((error) => {
        this.presentToast(error);
      });
  }

  getLogData() {
    const log = {} as Logs;
    log.amount = parseInt(this.f.coins.value);
    log.date = new Date();
    log.prev_balance = 0;
    log.new_balance = log.amount + log.prev_balance;
    return log;
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
