import { Component, OnInit } from '@angular/core';
import { DARK_MODE, OFF, ON } from '../../shared/constants/strings';
import { StorageService } from '../../shared/services/storage.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  darkMode: string;
  constructor(private readonly storage: StorageService) {}

  ngOnInit() {
    this.checkTheme();
  }

  checkTheme() {
    this.storage.getItem(DARK_MODE).then((darkMode) => {
      if (darkMode === ON) {
        this.darkMode = 'dark';
      } else {
        this.darkMode = 'light';
      }
    });
  }

  themeChanged(event) {
    if (event.detail.value === 'dark') {
      this.storage.setItem(DARK_MODE, ON);
      document.body.setAttribute('color-theme', 'dark');
    } else {
      this.storage.setItem(DARK_MODE, OFF);
      document.body.setAttribute('color-theme', 'light');
    }
  }
}
