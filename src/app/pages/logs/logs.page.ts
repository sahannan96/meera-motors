import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { doc, getFirestore, onSnapshot } from 'firebase/firestore';
import { CUSTOMER } from '../../shared/constants/strings';
import { Logs } from '../../shared/interfaces/list-data-interface';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.page.html',
  styleUrls: ['./logs.page.scss'],
})
export class LogsPage implements OnInit {
  db = getFirestore();
  uuid: string;
  logs: Logs[];
  spinner: boolean = true;
  constructor(private readonly route: ActivatedRoute) {}

  ngOnInit() {
    this.uuid = this.route.snapshot.params.id;
    this.loadLogs();
  }

  loadLogs() {
    const docRef = doc(this.db, CUSTOMER, this.uuid);
    onSnapshot(docRef, (doc) => {
      this.logs = [];
      this.logs = doc.data().logs;
      this.logs.sort((a, b) => b.date - a.date);
    });
    this.spinner = false;
    console.log(this.logs);
  }
}
