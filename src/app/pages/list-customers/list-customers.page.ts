import { Component, OnInit } from '@angular/core';
import {
  collection,
  getFirestore,
  onSnapshot,
  orderBy,
  query,
} from 'firebase/firestore';
import { CUSTOMER } from '../../shared/constants/strings';
import { Customer } from '../../shared/interfaces/list-data-interface';

@Component({
  selector: 'app-list-customers',
  templateUrl: './list-customers.page.html',
  styleUrls: ['./list-customers.page.scss'],
})
export class ListCustomersPage implements OnInit {
  db = getFirestore();
  spinner: boolean = true;
  customers: Customer[];
  filteredList: Customer[];
  constructor() {}

  ngOnInit() {
    this.loadCustomers();
  }

  loadCustomers() {
    const docRef = collection(this.db, CUSTOMER);
    const order = query(docRef, orderBy('name', 'asc'));
    onSnapshot(order, (snapshot) => {
      this.customers = [];
      snapshot.docs.forEach((doc) => {
        const customer = {} as Customer;
        customer.uuid = doc.id;
        customer.name = doc.get('name');
        customer.phone = doc.get('phone');
        customer.bike = doc.get('bike');
        customer.coins = doc.get('coins');
        this.customers.push(customer);
      });
      this.filteredList = this.customers;
      this.spinner = false;
    });
  }

  onSearchChange(event) {
    if (event.detail.value) {
      this.filteredList = this.customers.filter((item) => {
        return (
          new RegExp(event.detail.value, 'gi').test(item['name']) ||
          event.detail.value == ''
        );
      });
    } else {
      this.filteredList = this.customers;
    }
  }
}
