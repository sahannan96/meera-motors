import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'add-customer',
    loadChildren: () =>
      import('./pages/add-customer/add-customer.module').then(
        (m) => m.AddCustomerPageModule
      ),
  },
  {
    path: 'list-customers',
    loadChildren: () =>
      import('./pages/list-customers/list-customers.module').then(
        (m) => m.ListCustomersPageModule
      ),
  },
  {
    path: 'view-customer/:uuid',
    loadChildren: () =>
      import('./pages/view-customer/view-customer.module').then(
        (m) => m.ViewCustomerPageModule
      ),
  },
  {
    path: 'settings',
    loadChildren: () =>
      import('./pages/settings/settings.module').then(
        (m) => m.SettingsPageModule
      ),
  },
  {
    path: 'logs/:id',
    loadChildren: () =>
      import('./pages/logs/logs.module').then((m) => m.LogsPageModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
