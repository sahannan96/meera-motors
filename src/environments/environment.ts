// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  twilio: {
    sid: 'ACfe715ba91ee0c9e3f30f540e2b3cbaf1',
    token: 'c0aa2b328023cb4676fc66e3438d1c1c',
  },
  firebaseConfig: {
    apiKey: 'AIzaSyCzayBxWPpYOfmqpl5MWw0QoFZFxWtV6Qs',
    authDomain: 'meera-motors.firebaseapp.com',
    projectId: 'meera-motors',
    storageBucket: 'meera-motors.appspot.com',
    messagingSenderId: '1054997409125',
    appId: '1:1054997409125:web:4b7c5b47177ce44f6f458c',
    measurementId: 'G-YZ9S32PVK5',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
